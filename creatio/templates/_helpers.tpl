{{/*
Expand the name of the chart.
*/}}
{{- define "app.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Expand the name of the chart. Redis
*/}}
{{- define "app.redis.name" -}}
{{- if .Values.redis.nameOverride }}
{{- .Values.redis.nameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- print (include "app.name" .) "-redis" }}
{{- end }}
{{- end }}

{{/*
Expand the name of the chart. Postgres
*/}}
{{- define "app.postgres.name" -}}
{{- if .Values.postgres.nameOverride }}
{{- .Values.postgres.nameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- print (include "app.name" .) "-postgres" }}
{{- end }}
{{- end }}

{{/*
Expand the name of the chart. App
*/}}
{{- define "app.app.name" -}}
{{- if .Values.app.nameOverride }}
{{- .Values.app.nameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- print (include "app.name" .) "-app" }}
{{- end }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "app.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "app.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "app.labels" -}}
helm.sh/chart: {{ include "app.chart" . }}
{{ include "app.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Common labels Redis
*/}}
{{- define "app.redis.labels" -}}
helm.sh/chart: {{ include "app.chart" . }}
{{ include "app.redis.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Common labels Postgres
*/}}
{{- define "app.postgres.labels" -}}
helm.sh/chart: {{ include "app.chart" . }}
{{ include "app.postgres.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Common labels App
*/}}
{{- define "app.app.labels" -}}
helm.sh/chart: {{ include "app.chart" . }}
{{ include "app.app.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels Redis
*/}}
{{- define "app.redis.selectorLabels" -}}
app.kubernetes.io/name: {{ include "app.redis.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Selector labels Postgres
*/}}
{{- define "app.postgres.selectorLabels" -}}
app.kubernetes.io/name: {{ include "app.postgres.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Selector labels App
*/}}
{{- define "app.app.selectorLabels" -}}
app.kubernetes.io/name: {{ include "app.app.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "app.selectorLabels" -}}
app.kubernetes.io/name: {{ include "app.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}


{{/*
Create the name of the service account to use
*/}}
{{- define "app.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "app.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}
